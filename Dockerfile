FROM node:7.10.0
MAINTAINER eedkevin@gmail.com

WORKDIR /root
# copy source to docker container
COPY dist/app.tar /root/dist/app.tar

WORKDIR /root/src
RUN tar -xvzf /root/dist/app.tar -C /root/src

RUN npm install
RUN npm run build

EXPOSE 4080
ENV NODE_ENV=staging
ENTRYPOINT exec npm run start