### Requirements
 - node ^6.0.0
 - docker ^1.13.0
 - Internet connection
 
### Boot the code
#### Package source code
 1. package the source code for building docker image
 ```
 npm run package
 ```

#### Build docker image
 2. build the docker image locally
 ```
 docker build -t payment-gateway .
 ```
 
#### Lift it up
 3. lift up the service stack. Note that the command below requires docker version >= 1.13.0
 ```
 docker stack deploy --compose-file docker-compose.yml payment-gateway
 ```

### Try it
 - visit [docker service visualizer](http://localhost:8080) for services status check
 - visit [payment gateway home](http://localhost:4080) for everything about test submission
 
### For more ongoing progress
Please step to [my bitbucket repo](https://bitbucket.org/onmyon/payment-gateway-api)