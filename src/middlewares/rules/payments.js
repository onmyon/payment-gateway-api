function choosePlatform(req, res, next) {
  req.__params = req.__params || {};

  const paymentMethod = req.body.paymentMethod;
  const currency = req.body.currency;

  if(paymentMethod === 'AMEX' && currency !== 'USD') {
    req.__params.payment = {method: '', error: 'AMEX is possible to use only for USD'};
    return next();
  } else if(paymentMethod === 'AMEX') {
    req.__params.payment = {method: 'PayPal'};
    return next();
  } else {
    const payment = {};
    switch(currency) {
      case 'USD':
      case 'EUR':
      case 'AUD':
        payment.method = 'PayPal';
        break;
      default:
        payment.method = 'Braintree';
    }

    req.__params.payment = payment;
    return next();
  }
}

export default choosePlatform;