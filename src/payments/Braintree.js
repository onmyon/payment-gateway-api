import braintree from 'braintree';

import { config } from '../configs';

const gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  merchantId: config.payment.braintree.merchantId,
  publicKey: config.payment.braintree.publicKey,
  privateKey: config.payment.braintree.privateKey
});

export default class Braintree {
  async create(options) {
    const { price } = options;
    const nonce = 'fake-valid-nonce';

    return gateway.transaction.sale({
      amount: price,
      paymentMethodNonce: nonce,
      options: { submitForSettlement: true }
    });
  }

  async get(options) {
    const { paymentId } = options;
    return gateway.transaction.find(paymentId);
  }
}