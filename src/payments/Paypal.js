import paypal from 'paypal-rest-sdk';

import { config } from '../configs';

paypal.configure({
  'mode': config.payment.paypal.mode,
  'client_id': config.payment.paypal.clientId,
  'client_secret': config.payment.paypal.clientSecret,
});

export default class Paypal {
  async create(options) {
    const {
      customerName,
      currency,
      price,
      paymentMethod,
      cardHolderFirstName,
      carHolderLastName,
      carNumber,
      cardExpiration,
      cvv2
    } = options;

    const createPaymontJson = {
      intent: 'sale',
      payer: {
        payment_method: 'paypal'
      },
      redirect_urls: {
        return_url: config.payment.paypal.api.returnUrl,
        cancel_url: config.payment.paypal.api.cancelUrl
      },
      transactions: [{
        item_list: {
          items: [{
            name: 'item',
            sku: 'item',
            price: price,
            currency: currency,
            quantity: 1
          }]
        },
        amount: {
          currency: currency,
          total: price
        },
        description: 'This is the payment description.'
      }]
    };

    return new Promise((resolve, reject) => {
      paypal.payment.create(createPaymontJson, function (err, payment) {
        if (err) {
          reject(err);
        } else {
          resolve(payment);
        }
      });
    })
  }

  async get(options) {
    const { paymentId } = options;
    return new Promise((resolve, reject) => {
      paypal.payment.get(paymentId, function (err, payment) {
        if (err) {
          reject(err);
        } else {
          resolve(payment);
        }
      });
    });
  }
}