import { Router } from 'express';

const healthcheckRoute = Router();
healthcheckRoute.get('/', (req, res) => {
  res.status(200).send('ok');
});

export default healthcheckRoute;