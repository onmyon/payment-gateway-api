import homeRoute from './home-route';
import healthcheckRoute from './healthcheck-route';
import paymentRoute from './payment-route';

export {
  homeRoute,
  healthcheckRoute,
  paymentRoute,
}