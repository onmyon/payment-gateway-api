import { Router } from 'express';
import uuid from 'uuid/v4';

import { logger } from '../configs';
import { choosePlatform } from '../middlewares';
import { Paypal, Braintree } from '../payments';
import { PaymentDao } from '../daos';

const paymentRoute = Router();
const paypal = new Paypal();
const braintree = new Braintree();
const paymentDao = new PaymentDao();

paymentRoute.get('/', (req, res) => {
  res.render('payment/create');
});

paymentRoute.post('/', choosePlatform, async (req, res) => {
  try {
    if(!req.__params.payment.error) {
      let status;
      let payment = await paypal.create(req.body);
      switch(req.__params.payment.method) {
        case 'PayPal':
          payment = await paypal.create(req.body);
          status = payment.state;
          break;
        case 'Braintree':
          payment = await braintree.create(req.body);
          status = payment.success;
          break;
        default:
          throw new Error(`Payment Method not support. Method: [${req.__params.payment.method}]`);
      }

      if(!status || status === 'failed') {
        throw new Error('Your payment is unsucceeded');
      }

      const paymentRecord = await paymentDao.save({
                                paymentRefCode: uuid(),
                                order: req.body,
                                response: payment,
                              });

      res.render('payment/create-success', {paymentRefCode: paymentRecord.paymentRefCode});
    } else {
      throw new Error(req.__params.payment.error);
    }
  } catch (e) {
    logger.error(e);
    res.render('error', {error: e});
  }
});

paymentRoute.get('/check', (req, res) => {
  res.render('payment/check');
});

paymentRoute.post('/check', async (req, res) => {
  try {
    const { customerName, paymentRefCode } = req.body;
    const paymentRecord = await paymentDao.findOne({paymentRefCode: paymentRefCode});
    res.render('payment/check-success', {payment: !paymentRecord? undefined: paymentRecord.order});
  } catch (e) {
    logger.error(e);
    res.render('error', {error: e});
  }
});

export default paymentRoute;