function getCreditCardType(accountNumber) {
  // default
  var result = "unknown";

  // check for MasterCard
  if (/^5[1-5]/.test(accountNumber)) {
    result = "Mastercard";
  }

  // check for Visa
  else if (/^4/.test(accountNumber)) {
    result = "Visa";
  }

  // check for AmEx
  else if (/^3[47]/.test(accountNumber)) {
    result = "Amex";
  }

  return result;
}