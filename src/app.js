import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import helmet from 'helmet';
import path from 'path';

import { homeRoute, healthcheckRoute, paymentRoute } from './routes';
import { logger, config } from './configs';

const app = express();

app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
app.use(morgan('tiny'));
app.use(express.static(path.join(__dirname, 'public')));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use('/', homeRoute);
app.use('/health', healthcheckRoute);
app.use('/payment', paymentRoute);

app.listen(config.server.port, (err) => {
  if(!err) logger.info(`Payment Gateway API listening on port ${config.server.port}`);
  else logger.error('Error on starting Payment Gateway API');
});