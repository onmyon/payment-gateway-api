import redis from 'redis';
import bluebird from 'bluebird';

import { config } from '../../configs';

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const redisClient = redis.createClient(config.cache.redis.port, config.cache.redis.host);

export default redisClient;