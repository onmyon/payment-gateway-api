import mongoose from 'mongoose';
import bluebird from 'bluebird';

import { config } from '../../configs';

mongoose.Promise = bluebird;
mongoose.connect(config.db.mongo.connString);

export default mongoose;