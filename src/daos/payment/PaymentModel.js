import { mongoose } from '../driver';

const schema = new mongoose.Schema({
  paymentRefCode: String,
  order: Object,
  response: Object,
  isDeleted: {type: Boolean, default: false},
  createdAt: {type: Date, default: Date.now},
  updatedAt: {type: Date, default: Date.now},
}, {_id: true});

schema.pre('update', (next) => {
  this.update({}, {updatedAt: Date.now()});
  return next();
});

const PaymentModel = mongoose.model('payments', schema);

export default PaymentModel;