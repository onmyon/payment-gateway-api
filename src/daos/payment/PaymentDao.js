import PaymentModel from './PaymentModel';
import { redisClient } from '../driver';

export default class PaymentDao {
  async save(payment) {
    delete payment.order.cardNumber;
    delete payment.order.cardHolderFirstName;
    delete payment.order.cardHolderLastName;
    delete payment.order.cardExpiration;
    delete payment.order.cardCvv2;
    return new PaymentModel(payment).save();
  }

  async findOne(criteria) {
    const { paymentRefCode } = criteria;

    let payment = await redisClient.getAsync(paymentRefCode).then();
    payment = JSON.parse(payment);

    if(!payment) {
      payment = await PaymentModel.findOne(criteria, '-response').exec();
      await redisClient.setAsync(paymentRefCode, JSON.stringify(payment)).then();
    }

    return payment;
  }

  async delete(criteria) {
    return PaymentModel.update(criteria, {$set: {isDeleted: true}}).exec();
  }
}