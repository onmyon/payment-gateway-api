import { Logger, transports } from 'winston';
import fs from 'fs';

import env from '../../env.json';

const logDir = './logs/';
const logFile = logDir + 'app.log';

fs.stat(logDir, (err, stats) => {
  if(err) {
    console.log('log dir not exists, about to create it');
    fs.mkdir(logDir, () =>{});
  } else {
    if(!stats.isDirectory()) {
      console.log('log dir not exists, about to create it');
      fs.mkdir(logDir, ()=> {});
    }
  }
});

const logger = new Logger({
  transports: [
    new (transports.Console)({
      colorize: true,
      timestamp: true,
      level: "debug"
    }),
    new (transports.File)({
      filename: logFile,
      timestamp: true,
      json: false,
      level: "debug"
    })
  ]
});

const config = function() {
  const node_env = process.env.NODE_ENV || 'development';
  logger.info('loading app in [' + node_env + '] environment');
  return env[node_env];
}();

export {
  logger,
  config,
};