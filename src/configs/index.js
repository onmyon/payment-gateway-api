import { logger, config } from './config';

export {
  logger,
  config,
};